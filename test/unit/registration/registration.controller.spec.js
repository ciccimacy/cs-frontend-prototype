define(['angular', 'angular-mocks', 'registration/registration.module'], function () {
  'use strict';

  describe('Registration Controller', function () {
    var registrationEndpointService;

    beforeEach(module('registrationModule'));

    beforeEach(inject(function ($injector) {
      registrationEndpointService = $injector.get('registrationEndpointService');
    }));

    it("should call registrationEndpointService.register({user}) on register()",
      inject(function ($controller) {
        spyOn(registrationEndpointService, 'register');
        var controller = $controller('RegistrationController');

        controller.register();

        expect(registrationEndpointService.register).toHaveBeenCalled();
      })
    );
  });
});