define(['angular', 'angular-mocks', 'payroll/payroll.module'], function () {
  describe('Payroll Config', function () {
    'use strict';
    var $route, $q, payrollRoute, payrollService, payrollResolve;

    var payrollDetails = [{
      "employeeKey": "1",
      "dailyRate": 3181.82,
      "monthlyRate": 0.0,
      "hourlyRate": 0.0,
      "grossSalary": 57670.490000000005,
      "netSalary": 37067.7132,
      "taxRate": 0.32,
      "overtimePay": 397.73,
      "gasAllowance": 1000.0,
      "communicationAllowance": 1799.0,
      "riceAllowance": 1000.0,
      "clothingAllowance": 1500.0,
      "dateTo": "2015-08-08T00:00:00.000Z",
      "dateFrom": "2015-07-07T00:00:00.000Z",
      "deductions":
      {
        "incomeTaxDeduction": 18454.556800000002,
        "sssDeduction": 1024.22,
        "philHealthDeduction": 1024.0,
        "pagibigDeduction": 100.0,
        "absencesDeduction": 0.0,
        "totalDeduction": 20602.776800000003
      },
      "kind": "payroll#salariesItem"
    }];

    beforeEach(module('payrollModule'));

    beforeEach(inject(function ($injector) {
      $route = $injector.get('$route');
      $q = $injector.get('$q');
      payrollRoute = $route.routes['/payroll'];
      payrollService = $injector.get('payrollService');
      spyOn(payrollService, 'getAllPayrollDetails').and.returnValue(
          $q.when(payrollDetails));

    }));

    it('Should map routes to controllers and templateUrl', function(){
      expect(payrollRoute.controller).toBe('PayrollController');
      expect(payrollRoute.templateUrl).toMatch('modules/payroll/payroll.html');
    });

    it('Should return a result from the resolve block', inject(function($rootScope, $injector){
      payrollResolve = $injector.invoke(payrollRoute.resolve.payroll_details);

      $rootScope.$digest();

      expect(payrollRoute.resolve.payroll_details).toBeDefined();

      payrollResolve.then(function(data){
        expect(data).toEqual(payrollDetails);
      });
    }));
  });
});