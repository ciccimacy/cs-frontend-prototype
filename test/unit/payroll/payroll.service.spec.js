define(['angular', 'angular-mocks', 'payroll/payroll.module'], function () {
  describe('Payroll Service', function () {

    var $httpBackend, payrollService, payrollEndpointService, $q;

    beforeEach(module('payrollModule'));

    beforeEach(inject(function ($injector) {
      payrollService = $injector.get('payrollService');
      $httpBackend = $injector.get('$httpBackend');
      payrollEndpointService = $injector.get('payrollEndpointService');
      $q = $injector.get('$q');
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should get data from middleware endpoints using getAllPayrollDetails()', inject(function ($rootScope) {
      var payrollDetails = [{
            "employeeKey": "1",
            "dailyRate": 3181.82,
            "monthlyRate": 0.0,
            "hourlyRate": 0.0,
            "grossSalary": 57670.490000000005,
            "netSalary": 37067.7132,
            "taxRate": 0.32,
            "overtimePay": 397.73,
            "gasAllowance": 1000.0,
            "communicationAllowance": 1799.0,
            "riceAllowance": 1000.0,
            "clothingAllowance": 1500.0,
            "dateTo": "2015-08-08T00:00:00.000Z",
            "dateFrom": "2015-07-07T00:00:00.000Z",
            "deductions":
            {
              "incomeTaxDeduction": 18454.556800000002,
              "sssDeduction": 1024.22,
              "philHealthDeduction": 1024.0,
              "pagibigDeduction": 100.0,
              "absencesDeduction": 0.0,
              "totalDeduction": 20602.776800000003
            },
            "kind": "payroll#salariesItem"
      }];

      spyOn(payrollEndpointService, 'getAllPayrollDetails').and.returnValue(
          $q.when(payrollDetails));

      $rootScope.$digest();
      payrollService.getAllPayrollDetails(1).then(function (response){
        expect(response).toEqual(payrollDetails);
      });
    }));

    it('should get an empty response when passing a invalid ID to endpoint using getAllPayrollDetails()', function () {
      var payrollDetails = "";

      spyOn(payrollEndpointService, 'getAllPayrollDetails').and.returnValue(
          $q.when(payrollDetails));

      payrollService.getAllPayrollDetails('Invalid ID').then(function (response){
        expect(response).toEqual(payrollDetails);
      });
    });


  });

});