/**
 * Created by lbarretto on 8/5/15.
 */
define(['angular', 'angular-mocks', 'common/common.module'], function () {

  beforeEach(module('commonModule'));

    describe('Convert Date Filter', function(){

      var filter;

      beforeEach(function(){
        inject(function($injector){
          filter = $injector.get('$filter')('convertDate');
        });
      });

      it('should have a convertDate filter', function(){
        expect(filter).not.toBeNull();
      });

      it('should return a valid date from a valid input', function(){
          expect(filter('2015-01-01T00:00:00.000Z')).toBe('January 01, 2015');
      });

      it('should return "Invalid date" from an invalid input', function(){
        // must use iso string, utc timezone
        expect(filter('Invalid Input')).toBe('Invalid date');
      });

    });
});
