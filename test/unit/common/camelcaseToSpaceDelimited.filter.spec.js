/**
 * Created by lbarretto on 8/5/15.
 */
define(['angular', 'angular-mocks', 'common/common.module'], function () {

  beforeEach(module('commonModule'));

    describe('Camel Case to Space Delimited Filter', function(){

      var filter;

      beforeEach(function(){
        inject(function($injector){
          filter = $injector.get('$filter')('camelCaseToSpaceDelimited');
        });
      });

      it('should have a camelCaseToSpaceDelimited filter', function(){
        expect(filter).not.toBeNull();
      });

      it('should have return a space delimited string', function(){
        expect(filter('camelCaseString')).toEqual('Camel Case String');
      });

      it('should have return "Invalid String" given a non-string value', function(){
        expect(filter(1234)).toEqual('Invalid String Input');
      });

    });
});
