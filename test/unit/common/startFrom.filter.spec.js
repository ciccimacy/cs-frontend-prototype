/**
 * Created by lbarretto on 8/5/15.
 */
define(['angular', 'angular-mocks', 'common/common.module'], function () {

  beforeEach(module('commonModule'));

    describe('Start From Filter', function(){

      var filter;
      var initial_data = [{
        "employeeKey": "1",
        "dailyRate": 3181.82,
        "monthlyRate": 0
      },
        {
          "employeeKey": "2",
          "dailyRate": 3181.82,
          "monthlyRate": 0
        },
        {
          "employeeKey": "3",
          "dailyRate": 3181.82,
          "monthlyRate": 0
        },
        {
          "employeeKey": "4",
          "dailyRate": 3181.82,
          "monthlyRate": 0
        }];

      beforeEach(function(){
        inject(function($injector){
          filter = $injector.get('$filter')('startFrom');
        });
      });

      it('should have a startFrm filter', function(){
        expect(filter).not.toBeNull();
      });

      it('should return a valid sliced data given an index less than the length of the data being sliced', function(){
        var sliced_data = [{
            "employeeKey": "3",
            "dailyRate": 3181.82,
            "monthlyRate": 0
          },
          {
            "employeeKey": "4",
            "dailyRate": 3181.82,
            "monthlyRate": 0
          }];

        expect(filter(initial_data, 2)).toEqual(sliced_data);
      });

      it('should return an an empty list if index is greater than the length of the data being sliced', function(){
        expect(filter(initial_data, 5)).toEqual([]);
      });

    });
});