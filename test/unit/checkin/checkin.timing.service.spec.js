define(['moment','angular','angular-mocks','checkin/checkin.module'], function(moment) {
  'use strict';

  describe('CheckinTimingService', function () {

    var checkinTimingService, endpointsService, authenticationService, $rootScope, $interval, $q;

    // load the controller's module
    beforeEach(module('checkinModule'));
    beforeEach(module('authenticationModule'));

    // Initialize the controller and mock interval
    beforeEach(inject(function ($injector){
      $interval = $injector.get('$interval');
      $rootScope = $injector.get('$rootScope');
      $q = $injector.get('$q');

      endpointsService = $injector.get('checkinEndpointsService');
      spyOn(endpointsService,'timeIn').and.returnValue($q.when({
        status:603
      }));
      spyOn(endpointsService,'timeOut').and.returnValue($q.when({
        status:703
      }));

    }));

    describe('given that the user has not timed in yet', function (){

      beforeEach(inject(function ($injector){
        spyOn(endpointsService,'getStatus').and.returnValue($q.when({
          timeInStatus:601
        }));
        checkinTimingService = $injector.get('checkinTimingService');
        $rootScope.$digest();
      }));

      it('should have a clock that contains the current time', function() {
        var e = 10;
        expect(checkinTimingService.clock).toBeDefined();
        expect(Math.abs(checkinTimingService.clock.getTime()-new Date().getTime())).toBeLessThan(e);
      });

      it('should have a status and enable users to time in and time out', function () {
        expect(checkinTimingService.status.message).toBeDefined();
        expect(checkinTimingService.timeIn).toBeDefined();
        expect(checkinTimingService.timeOut).toBeDefined();
      });

      it('should have status IDLE when user is not timed in, RUNNING when he is timed in', function () {
        checkinTimingService.timeIn();
        $rootScope.$digest();
        expect(checkinTimingService.status.message).toBe('RUNNING');
        checkinTimingService.timeOut();
        $rootScope.$digest();
        expect(checkinTimingService.status.message).toBe('IDLE');
      });

      it('should record starting time on time in and ending time on time out', function () {
        checkinTimingService.timeIn();
        expect(checkinTimingService.startTime).toBeTruthy();
        checkinTimingService.timeOut();
        expect(checkinTimingService.endTime).toBeTruthy();
        $rootScope.$digest();
      });

      it('should pass employeeId, Date and Time to endpoint service on time in', function () {
        checkinTimingService.timeIn();
        $rootScope.$digest();
        expect(endpointsService.timeIn).toHaveBeenCalledWith(jasmine.objectContaining({
          date: moment(checkinTimingService.startTime).format("YYYY-MM-DD"),
          time: moment(checkinTimingService.startTime).format("HH:mm:ss")
        }));
      });

      it('should pass employeeId, Date and Time to endpoint on time out', function () {
        checkinTimingService.timeOut();
        $rootScope.$digest();
        expect(endpointsService.timeOut).toHaveBeenCalledWith(jasmine.objectContaining({
          date: moment(checkinTimingService.endTime).format("YYYY-MM-DD"),
          time: moment(checkinTimingService.endTime).format("HH:mm:ss")
        }));
      });

      it('should record running time from when the user timed in', function () {
        checkinTimingService.timeIn();
        $rootScope.$digest();
        var startTime = checkinTimingService.startTime.getTime();
        var e = 10;
        var duration = 5000;

        jasmine.clock().install();
        jasmine.clock().mockDate(checkinTimingService.clock);
        jasmine.clock().tick(duration);
        $interval.flush(1000);
        expect(Math.abs(checkinTimingService.runningTime-duration)).toBeLessThan(e);
        jasmine.clock().uninstall();
      });
    });

    describe('given that the user has already timed in', function (){

      beforeEach(inject(function ($injector){
        spyOn(endpointsService,'getStatus').and.returnValue($q.when({
          timeInStatus:600
        }));
        checkinTimingService = $injector.get('checkinTimingService');
        $rootScope.$digest();
      }));

      it('should have status RUNNING after initialization', function () {
        expect(checkinTimingService.status.message).toBe('RUNNING');
      });
    });

  });
});
