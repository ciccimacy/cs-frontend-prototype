define(['moment','angular','angular-mocks','attendance/attendance.module'], function(moment) {
  'use strict';

  describe('AttendanceRecordsService', function () {

    var attendanceRecordsService, endpointsService, authenticationService, $rootScope, $q;

    // load the controller's module
    beforeEach(module('attendanceModule'));
    beforeEach(module('authenticationModule'));

    // Initialize the controller and mock interval
    beforeEach(inject(function ($injector){
      $rootScope = $injector.get('$rootScope');
      $q = $injector.get('$q');

      endpointsService = $injector.get('attendanceEndpointsService');
      spyOn(endpointsService,'getRecords').and.returnValue($q.when(
        [
          {
            id:"id1",
            employeeId:"12423",
            date: "2015-08-11T00:00:00.000Z",
            timeIn: [
              "02:24:46",
              "02:55:27",
              "18:35:46",
              "19:40:50"
            ],
            timeOut: [
              "02:24:53",
              "18:35:43",
              "18:39:56"
            ]
          },
          {
            id:"id2",
            employeeId: "12423",
            date: "2015-08-12T00:00:00.000Z",
            timeIn: [
            "12:46:31",
            "12:46:59",
            "13:22:18"
            ],
            timeOut: [
            "12:46:47",
            "12:47:05",
            "13:22:25"
            ]
          }
        ]
      ));

      attendanceRecordsService = $injector.get('attendanceRecordsService');
    }));

    it('should convert records from endpoint to calendar events format', function(){
      attendanceRecordsService.getRecords().then(function(response){
        expect(response[0].type).toEqual(jasmine.any(String));
        expect(response[0].startsAt).toEqual(jasmine.any(Date));
      });
      $rootScope.$digest();
      expect(endpointsService.getRecords).toHaveBeenCalled();
    });
  });
});
