define(['angular', 'angular-mocks', 'contract/contract.module'], function () {
  describe('Contract Controller', function () {
    'use strict';

    var contractService;
    var contractController;

    var returnContractDetail = {
              "id": "5668600916475904",
               "title": "Bank Transfer Contract v01",
               "summary": "This is the summary of your contract. Place pertinent information here.",
               "skillsList": [
                "Weapons Handling Skill",
                "Driving Skill",
                "Self-defense Skill"
               ],
               "customerId": "4",
               "accountList": [
                  {
                   "name": "Account Name 001",
                   "assignment": {
                      "name": "Bank Cash Transfer"
                   },
                   "workOrderList": [
                      {
                       "type": "Bank Transfer Order"
                      },
                      {
                       "type": "Another Bank Transfer Order"
                      }
                   ]
                  },
                  {
                   "name": "Account Name 002",
                   "assignment": {
                      "name": "Second Assignment Name"
                   }
                  }
               ],
               "kind": "contractapi#contractItem",
               "etag": "\"29BVa5dTiL83IiqJapwNOqM3yuU/quACNgE53zQbNPHxzRhCKAcBAbA\""
    };

    beforeEach(module('contractModule'));

    beforeEach(inject(function ($q, $controller, $injector) {
      contractService = $injector.get('contractService');
      spyOn(contractService, 'getCustomerContractDetails').and.returnValue(
        $q.when(returnContractDetail));
      contractController = $controller('ContractController');
    }));

    it('should call the getCustomerContractDetails of the service onpage load', inject(function ($q,$rootScope) {

      contractController.contractId = "5668600916475904";
      contractController.customerId = "4";
      contractController.getCustomerContractDetails();
      $rootScope.$digest();

      expect(contractController).toBeDefined();
      expect(contractController.contractDetail).toEqual(returnContractDetail);
      expect(contractController.contractId).toEqual("5668600916475904");
      expect(contractController.contractDetail.skillsList)
        .toEqual(jasmine.arrayContaining(["Driving Skill"]));
      expect(contractController.contractDetail.title).toEqual("Bank Transfer Contract v01");
      expect(contractService.getCustomerContractDetails).toHaveBeenCalledWith("4","5668600916475904");

    }));

    it('should route to the correct page', inject(function ($route) {

     expect($route.routes['/contract-details/:customerId/:contractId/:pageFrom'].templateUrl)
     .toMatch('modules/contract/contract-detail.html');

     expect($route.routes['/contract-details/:customerId/:contractId/:pageFrom'].controller)
     .toEqual('ContractController');

    }));

  });
});
