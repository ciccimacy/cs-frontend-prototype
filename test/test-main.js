'use strict';

var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

// Get a list of all the test files to include
Object.keys(window.__karma__.files).forEach(function (file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    // If you require sub-dependencies of test files to be loaded as-is (requiring file extension)
    // then do not normalize the paths
    //var normalizedTestModule = file.replace(/^\/base\/|\.js$/g, '');
    allTestFiles.push(file);
  }
});

require.config({
  paths: {
    cryptojs: '//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256',
    gapi: '//apis.google.com/js/client',
    angular: '../../bower_components/angular/angular',
    'angular-route': '../../bower_components/angular-route/angular-route',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
    jquery: '../../bower_components/jquery/dist/jquery',
    'angular-ui-select': '../../bower_components/angular-ui-select/dist/select',
    'angular-mocks': '../../node_modules/angular-mocks/angular-mocks',
    'angular-sanitize': '../../bower_components/angular-sanitize/angular-sanitize',
    'convertDate-filter': 'common/convertDate.filter',
    'startFrom-filter': 'common/startFrom.filter',
    'camelCaseToSpaceDelimited-filter': 'common/camelCaseToSpaceDelimited.filter',
    checkinEndpoints: 'checkin/checkin.mock.endpoints.service',
    customerEndpoints: 'customer/customer.mock.endpoint.service',
    contractEndpoints: 'contract/contract.mock.endpoint.service',
    attendanceEndpoints: 'attendance/attendance.mock.endpoints.service',
    'angular-bootstrap-calendar': '../../bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls',
    moment: '../../bower_components/moment/moment',
    interact: '../../bower_components/interact/interact',
    'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
    async: '../../bower_components/requirejs-plugins/src/async',
    depend: '../../bower_components/requirejs-plugins/src/depend',
    font: '../../bower_components/requirejs-plugins/src/font',
    goog: '../../bower_components/requirejs-plugins/src/goog',
    image: '../../bower_components/requirejs-plugins/src/image',
    json: '../../bower_components/requirejs-plugins/src/json',
    mdown: '../../bower_components/requirejs-plugins/src/mdown',
    noext: '../../bower_components/requirejs-plugins/src/noext',
    propertyParser: '../../bower_components/requirejs-plugins/src/propertyParser',
    'Markdown.Converter': '../../bower_components/requirejs-plugins/lib/Markdown.Converter',
    text: '../../bower_components/requirejs-plugins/lib/text',
    underscore: '../../bower_components/underscore/underscore',
    'payroll-endpoint-service': 'payroll/payroll.mock.endpoint.service'
  },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'angular-ui-select': [
      'angular'
    ],
    'angular-mocks': [
      'angular'
    ],
    'angular-sanitize': [
      'angular'
    ],
    'angular-bootstrap': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    cryptojs: {
      exports: 'CryptoJS'
    }
  },

  baseUrl: '/base/app/modules',

  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
