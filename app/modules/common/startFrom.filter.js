define([], function(){
  'use strict';

  // Used for pagination, to filter data
  function startFrom(){
    return function (input, start) {
      if (input) {
        var result = input.slice(start);
        return result;
      }
      return [];
    };
  }

  return startFrom;
});
