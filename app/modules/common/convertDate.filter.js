define(['moment'], function(moment){
  'use strict';

  function convertDate(){
    return function(input){
      // different formats of input can be specified
      var result = moment(input, [moment.ISO_8601]).format('MMMM DD, YYYY');
      return result;
    };
  }

  return convertDate;
});
