define([], function(){
  'use strict';

  function camelCaseToSpaceDelimited(){
    return function(input){
      // convert camel case to human readable format

      var result = "Invalid String Input";

      if (typeof input === 'string' || input instanceof String) {
        result = input.charAt(0).toUpperCase() + input.substr(1).replace(/[A-Z]/g, ' $&');
      }

      return result;
    };
  }

  return camelCaseToSpaceDelimited;
});
