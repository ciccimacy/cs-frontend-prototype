define([], function () {
  'use strict';

  function messageDirective() {
    return {
      scope: {
              alertType:'=',
              alertMessage: '='
            },
      restrict: 'E',
      templateUrl: 'modules/common/common-message.partial.html'
    };
  }

  messageDirective.$inject = [];

  return messageDirective;
});