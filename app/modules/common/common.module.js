define([ 
'angular', 
'convertDate-filter', 
'startFrom-filter',
'camelCaseToSpaceDelimited-filter',
'./common-message.partial',
'./loading-page.partial'
], function(angular, convertDateFilter, startFromFilter, camelCaseToSpaceDelimitedFilter, messageDirective, loadingDirective){
  angular.module('commonModule', [])
      .filter('convertDate', convertDateFilter)
      .filter('startFrom', startFromFilter)
      .filter('camelCaseToSpaceDelimited', camelCaseToSpaceDelimitedFilter)
      .directive('message', messageDirective)
      .directive('loadPage', loadingDirective);
});
