require.config({
  paths: {
    angular: '../../bower_components/angular/angular',
    'angular-route': '../../bower_components/angular-route/angular-route',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
    jquery: '../../bower_components/jquery/dist/jquery',
    'angular-ui-select': '../../bower_components/angular-ui-select/dist/select',
    'angular-sanitize': '../../bower_components/angular-sanitize/angular-sanitize',
    'angular-mocks': '../../node_modules/angular-mocks/angular-mocks',
    'angular-bootstrap-calendar': '../../bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls',
    moment: '../../bower_components/moment/moment',
    interact: '../../bower_components/interact/interact',
    'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'convertDate-filter': 'common/convertDate.filter',
    'startFrom-filter': 'common/startFrom.filter',
    'camelCaseToSpaceDelimited-filter': 'common/camelCaseToSpaceDelimited.filter',
    checkinEndpoints: 'checkin/checkin.mock.endpoints.service',
    customerEndpoints: 'customer/customer.endpoint.service',
    contractEndpoints: 'contract/contract.endpoint.service',
    attendanceEndpoints: 'attendance/attendance.mock.endpoints.service',
    async: '../../bower_components/requirejs-plugins/src/async',
    depend: '../../bower_components/requirejs-plugins/src/depend',
    font: '../../bower_components/requirejs-plugins/src/font',
    goog: '../../bower_components/requirejs-plugins/src/goog',
    image: '../../bower_components/requirejs-plugins/src/image',
    json: '../../bower_components/requirejs-plugins/src/json',
    mdown: '../../bower_components/requirejs-plugins/src/mdown',
    noext: '../../bower_components/requirejs-plugins/src/noext',
    propertyParser: '../../bower_components/requirejs-plugins/src/propertyParser',
    'Markdown.Converter': '../../bower_components/requirejs-plugins/lib/Markdown.Converter',
    text: '../../bower_components/requirejs-plugins/lib/text',
    underscore: '../../bower_components/underscore/underscore',
    'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
    'payroll-endpoint-service': 'payroll/payroll.mock.endpoint.service',
    'angularjs-camelCase-human': '../../bower_components/angularjs-camelCase-human/camelCaseToHuman',
    nprogress: '../../bower_components/nprogress/nprogress',
    'angular-animate': '../../bower_components/angular-animate/angular-animate'
  },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'angular-ui-select': [
      'angular',
      'angular-sanitize'
    ],
    'angular-sanitize': {
      deps: [
        'angular'
      ]
    },
    'angular-mocks': [
      'angular'
    ],
    'angular-bootstrap': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    'angular-bootstrap-calendar': [
      'angular',
      'moment',
      'interact',
      'angular-bootstrap'
    ],
    'angular-animate': [
      'angular'
    ]
  },
  packages: [

  ]
});
require([
  'nprogress',
  'bootstrap',
  'angular',
  'angular-sanitize',
  'common/common.module',
  'navigation/navigation.module',
  'customer/customer.module',
  'checkin/checkin.module',
  'registration/registration.module',
  'attendance/attendance.module',
  'payroll/payroll.module',
  'contract/contract.module',
  'angular-animate',
  'async!https://apis.google.com/js/client.js!onload'
], function (NProgress) {
  'use strict';

  NProgress.configure({showSpinner: false});

  bootstrap();
  //Load Google APIs here
  function loadGoogleApis() {
    var apiList = [
      {name: 'oauth2', version: 'v2'},
      {name: 'middleware', version: 'v1', url: 'https://gae-modules-poc.appspot.com/_ah/api'}
    ];

    var apisToLoad = apiList.length;
    for (var i = 0; i < apiList.length; i++) {
      var api = apiList[i];
      NProgress.inc();
      console.log('Loading ' + api.name + '...');
      gapi.client.load(api.name, api.version, loadedApiCallback, api.url);
    }

    function loadedApiCallback() {
      if (--apisToLoad === 0) {
        NProgress.done();
        console.log("Endpoints Service is ready!");
      }
    }
  }

  function bootstrap() {
    console.log('Bootstrapping modules...');
    NProgress.inc();
    angular.bootstrap(document, ['ngAnimate', 'ngSanitize', 'registrationModule', 'navigationModule', 'customerModule', 'checkinModule', 'attendanceModule', 'payrollModule', 'contractModule']);
    loadGoogleApis();
    console.log('Modules bootstrapped!');
  }

});
