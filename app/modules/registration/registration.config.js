define(['require'], function (require) {
  'use strict';

  function config($routeProvider) {
    $routeProvider.when('/employees', {
      templateUrl: require.toUrl('./registration.html'),
      controller: 'RegistrationController',
      controllerAs: 'vm'
    });
  }

  config.$inject = ['$routeProvider'];

  return config;
});
