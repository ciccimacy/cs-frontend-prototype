define([], function () {
  'use strict';

  attendanceEndpointsService.$inject = ['$q'];

  function attendanceEndpointsService ($q) {
    var es = this;

    es.getRecords = getRecords;

    function getRecords(payload){
      console.log(payload);
      var p = $q.defer();
      gapi.client.middleware.timekeeping.employees.timerecords.list(payload).then(function(response){
        p.resolve(JSON.parse(response.body).timeKeepingList);
  		},function(response){
        p.reject("Service not found.");
      });
      return p.promise;
    }
  }

  return attendanceEndpointsService;
});
