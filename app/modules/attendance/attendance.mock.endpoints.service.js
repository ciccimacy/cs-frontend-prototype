define(['moment'], function (moment) {
  'use strict';

  attendanceEndpointsService.$inject = ['$q','$timeout'];

  function attendanceEndpointsService ($q, $timeout) {
    var es = this;

    es.getRecords = getRecords;
    ////////////////////

    function getRecords(payload){
      var p = $q.defer();
      //variable month based on payload to simulate events every month
      var month = payload.dateFrom.substr(0,7);
      var startMoment = moment(month,'YYYY-MM-DD').startOf('month');
      var endMoment = startMoment.clone().add(15,'d');
      var records = [];
      for(var i = 0; startMoment.isBefore(endMoment); i++){
        records.push(randomTimeRecordFactory('event'+i,startMoment.format('YYYY-MM-DD')));
        startMoment.add(1,'d');
      }
        p.resolve(records);
  		return p.promise;
    }

    function randomTimeRecordFactory(id,date){
      var timeRecord = {};
      var someStartTime = (Math.random() * 23) + 1;
      timeRecord.id = id;
      timeRecord.employeeId = "12423";
      timeRecord.date = date + "T00:00:00.000Z";
      timeRecord.timeIn = [
        someStartTime + ":00:00"
      ];
      timeRecord.timeOut = [
        ((someStartTime + 8) > 23 ? 23 : someStartTime + 8) + ":00:00"
      ];

      return timeRecord;
    }
  }

  return attendanceEndpointsService;
});
