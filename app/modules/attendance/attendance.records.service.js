define(['moment'], function (moment) {
  'use strict';

  attendanceRecordsService.$inject = ['$q', '$interval', 'attendanceEndpointsService'];

  function attendanceRecordsService ($q, $interval, endpointsService) {
    var service = this;

    service.getRecords = getRecords;

    init();
    ////////////////////

    function init(){

    }

    //transforms JSON list data from endpoint to event list format for calendar
    function getRecords(payload){
      var p = $q.defer();
      endpointsService.getRecords(payload).then(function(items){
        if(items){
          var eventList = [];
          for(var i = 0; i < items.length; i++){
            for(var j = 0; j < items[i].timeIn.length; j++){
              var event = {};
              var record = items[i];

              event.type = 'info';
              if(items[i].timeIn[j]){
                var startDateTimeStr = record.date.substr(0,10) + ' ' + items[i].timeIn[j];
                event.startsAt = moment(startDateTimeStr,'YYYY-MM-DD HH:mm:ss').toDate();
              }
              if(items[i].timeOut[j]){
                var endDateTimeStr = record.date.substr(0,10) + ' ' + items[i].timeOut[j];
                event.endsAt = moment(endDateTimeStr,'YYYY-MM-DD HH:mm:ss').toDate();
              }
              event.title = '[' + moment(event.startsAt).format('HH:mm') + ' - ' + moment(event.endsAt).format('HH:mm') + ']';
              eventList.push(event);
            }
          }
          p.resolve(eventList);
        }else{
          p.reject('Bad service response.');
        }
      },function(error){
        p.reject(error);
      });

      return p.promise;
    }

  }

  return attendanceRecordsService;
});
