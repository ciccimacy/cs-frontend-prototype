define([], function () {
  'use strict';

  controller.$inject = ['contractService','$routeParams','$location'];

  function controller(service,routeParams,location) {
    var vm = this;
    vm.getCustomerContractDetails = getCustomerContractDetails;
    vm.goBackToContractList = goBackToContractList;
    vm.contractId = routeParams.contractId;
    vm.customerId = routeParams.customerId;
    vm.pageFrom = routeParams.pageFrom;
    vm.contractDetail = {};
    vm.dataLoaded = false;

    activate();

    function activate() {
      getCustomerContractDetails();
    }

    function getCustomerContractDetails() {
        return service.getCustomerContractDetails(vm.customerId, vm.contractId).then(function (data) {
          vm.contractDetail = data;

          if(vm.contractDetail.error){  
            vm.errorType = "alert-danger";
          }  
          
          return vm.contractDetail;
        }).finally(function(data){
          vm.dataLoaded = true;
        });

    }

    function goBackToContractList(){
      var URL = '/customer';
      if(vm.contractDetail.customerId !== undefined || vm.contractDetail.customerId !== null){
        URL += '/' +  vm.contractDetail.customerId;
      }

      if(vm.pageFrom){
        URL += '/' + vm.pageFrom;
      }

      location.path(URL);
    }

  }

  return controller;
});