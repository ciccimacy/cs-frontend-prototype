define(['require'], function (require) {
  'use strict';

  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider.when('/contract-details/:customerId/:contractId/:pageFrom', {
      templateUrl: require.toUrl('./contract-detail.html'),
      controller: 'ContractController',
      controllerAs: 'vm'
    });
  }

  return config;
});
