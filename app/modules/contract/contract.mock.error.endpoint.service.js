define([], function () {
  'use strict';
  
  contractEndpointService.$inject = ['$q'];

  function contractEndpointService ($q) {
    var _this = this;

    _this.getContractDetail = getContractDetail;

    var errorMessage = {"result":{
       "error": {
        "errors": [
         {
          "domain": "global",
          "reason": "backendError",
          "message": "503 Over Quota [Sample Error Message]"
         }
        ],
        "code": 503,
        "message": "503 Over Quota [CONTRACT Sample Error Message]"
       }
      }
    };
    
    function getContractDetail(customerId, contractId){
          var p = $q.defer();
          p.reject(errorMessage);
      		return p.promise;
    }
  }

  return contractEndpointService;
});
