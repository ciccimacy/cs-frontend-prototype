define(['cryptojs'], function (CryptoJS) {
  'use strict';

  controller.$inject = ['loginEndpointService', 'authenticationService', '$location', '$window'];

  function controller(loginEndpointService, authenticationService, $location, $window) {
    var _this = this;
    _this.loginEmail = undefined;
    _this.password = undefined;
    _this.loginUser = loginUser;

    function loginUser() {
      loginEndpointService.login(_this.loginEmail, CryptoJS.SHA256(_this.password).toString(CryptoJS.enc.Base64)).then(function (response) {
        if (response.data) {
          authenticationService.auth(response.data.id);
          redirectToHome(true);
        } else {
          authenticationService.logout();
          redirectToHome(false);
        }
      });
    }

    function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec($window.location.href);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function redirectToHome(stat) {
      if (stat) {
        $window.location.href = $location.protocol() + "://" + location.host + "/#" + getParameterByName('next');
      } else {
        _this.loginFailed = true;
      }
    }
  }

  return controller;
});