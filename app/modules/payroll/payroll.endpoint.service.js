define([], function () {
  'use strict';

  function payrollEndpointService ($q) {

    return {
      getAllPayrollDetails : function(user_id){
          var p = $q.defer();

          gapi.client.payroll.employees.salaries.list({'id' : user_id})
                                          .then(function(response){
                                            console.log("Successss: " + response);
                                            if(response.status !== 200){
                                              p.resolve("");
                                            }
                                            p.resolve(response.result.items);
                                          },function(response){
                                            console.log("Fail: " + response);
                                            p.reject("Service Unavailable");
                                          });
      		return p.promise;
      }
    };
  }

  payrollEndpointService.$inject = ['$q'];

  return payrollEndpointService;
});
