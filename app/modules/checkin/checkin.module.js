  define([
  'angular',
  'angular-route',
  './checkin.config',
  './checkin.core.controller',
  'checkinEndpoints',
  './checkin.timing.service',
  'authentication/authentication.module'
], function(angular, angularRoute, config, coreController, endpointsService, timingService, authModule) {
    angular
      .module('checkinModule', ['ngRoute'])
      .config(config)
      .controller('CheckinCoreController', coreController)
      .service('checkinEndpointsService', endpointsService)
      .service('checkinTimingService',timingService);
});
