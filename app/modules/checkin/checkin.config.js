define(['require'], function(require) {
  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider
      .when('/checkin', {
        templateUrl: require.toUrl('./checkin.clock.partial.html'),
        controller: 'CheckinCoreController',
        loginRequired:true,
        controllerAs: 'cc'
      });
  }

  return config;
});
