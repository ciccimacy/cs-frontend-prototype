define([], function () {
  'use strict';
  
  customerEndpointService.$inject = ['$q'];

  function customerEndpointService ($q) {

    var _this = this;

    _this.getCustomers = getCustomers;
    _this.getContracts = getContracts;

    function getCustomers(){
        var p = $q.defer();
        gapi.client.middleware.customers.list()
                                        .then(function(response){
                                          console.log("Success: " + response);
                                          p.resolve(response);
                                        },function(response){
                                          console.log("Fail: " + response);
                                          p.reject(response);
                                        });
    		return p.promise;
    }

    function getContracts(customerId){
          var p = $q.defer();
          gapi.client.middleware.contract.list({'customer_id' : customerId})
                                          .then(function(response){
                                            console.log("Success: " + JSON.stringify(response));
                                            p.resolve(response);
                                          },function(response){
                                            console.log("Fail: " + JSON.stringify(response));
                                            p.reject(response);
                                          });
          return p.promise;
    }
  }

  return customerEndpointService;
});
