define([], function () {
  'use strict';

  controller.$inject = ['customerService', '$routeParams'];

  function controller(service,routeParam) {
    var vm = this;
    vm.customers = [];
    vm.selectedCustomer = {
        id: null,
        name: null
      };
    vm.contracts = [];
    vm.customerId = routeParam.customer_id;
    
    //functions
    vm.getCustomers = getCustomers;
    vm.getContracts = getContracts;
    vm.setIndex = setIndex;

    // for loading screen
    vm.dataLoaded = false;
    vm.contractLoaded =false;

    //----PAGING----
    vm.numPerPage = 6;

    function setIndex(index){
       var idx = index;
       if(vm.currentPage > 1){
          idx = (vm.currentPage-1)*vm.numPerPage;
       }
       return idx;
    }
    //-----------

    activate();

    function activate() { 
      getCustomers();

      if(vm.customerId){
        getContracts(vm.customerId, false);        
      }

      vm.currentPage = routeParam.page ? routeParam.page : 1;
    }

    function getCustomers() {
      return service.getCustomers().then(function (data) {
        vm.customers = data;

        if(!vm.customers.error){    
          if(vm.customerId){
            var idx = service.getCustomerById(vm.customerId, vm.customers.items);
            vm.selectedCustomer.selected = vm.customers.items[idx];
          }
        }else{
          vm.errorType = "alert-danger";
        }
       
        return vm.customers;
      }).finally(function(data){
        vm.dataLoaded = true; 
      });
    }
    
    function getContracts(customerId, fromSelect) {

        vm.contractLoaded = false;

        if(vm.selectedCustomer){
          return service.getContracts(customerId).then(function (data) {
            vm.contracts = data;

            if(!vm.contracts.error){   
             
              vm.totalItems = vm.contracts.items.length;
              vm.noOfPages = Math.ceil(vm.totalItems / vm.numPerPage);

              if(fromSelect){
                vm.currentPage = 1;
              }
            }

            return vm.contracts;
          }).finally(function(data){
            vm.contractLoaded = true; 
          });
        }
    }
  }

  return controller;
});