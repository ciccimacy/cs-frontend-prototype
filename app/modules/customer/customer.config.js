define(['require'], function (require) {
  'use strict';

  function config($routeProvider) {
    $routeProvider.when('/customer', {
      templateUrl: require.toUrl('./customer.html'),
      controller: 'CustomerController',
      controllerAs: 'vm'
    }).when('/contracts', {
      templateUrl: require.toUrl('./customer.html'),
      controller: 'CustomerController',
      controllerAs: 'vm'
    }).when('/customer/:customer_id/:page', {
      templateUrl: require.toUrl('./customer.html'),
      controller: 'CustomerController',
      controllerAs: 'vm'
    });
  }

  config.$inject = ['$routeProvider'];

  return config;
});
