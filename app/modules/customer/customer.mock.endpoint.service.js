define([], function () {
  'use strict';
  
  customerEndpointService.$inject = ['$q', '$timeout'];

  function customerEndpointService ($q, $timeout) {
    var _this = this;

    _this.getCustomers = getCustomers;
    _this.getContracts = getContracts;

    function returnCommonData(){
      var contract = {
             "summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean volutpat vel quam vitae venenatis.",
             "skillsList": [
              "Top Skill",
              "Weapons Management",
              "Self-defense Skills",
              "Language Skills"
             ],
             "serviceLine": {
              "name": "Service Line of Contract"
             },
             "accountList": [
              {
               "name": "Account_01",
               "assignment": {
                "name": "Account_01 Assignment"
               },
               "workOrderList": [
                {
                 "type": "Deliver the Money to BPI"
                },
                {
                 "type": "Deliver the Money to BDO"
                }
               ]
              }
             ],
             "kind": "contracts#contractsItem"
          }; 
        return contract;
    }

   function generateContract(customerId){
      var contracts = {
        "result" : {
          "items" : []
        } 
      };

      if(customerId !== "113"){
        for (var i = 0; i <= 7; i++) {
          var contract = returnCommonData();
          
          contract["id"] = "C" + i;
          contract["customerId"] = customerId;
          contract["title"] = "Contract " + customerId + "_C" + i;

          contracts.result.items.push(contract);
        }
      }
      
      return contracts;
    }

    function getCustomers(){
        var p = $q.defer();

        $timeout(function() {
            p.resolve({
            "result" : {
               "items": [
                {
                 "id": "111",
                 "name": "Customer111",
                 "kind": "customers#customersItem"
                },
                {
                 "id": "112",
                 "name": "Customer112",
                 "kind": "customers#customersItem"
                },
                {
                 "id": "113",
                 "name": "Customer113",
                 "kind": "customers#customersItem"
                }
               ],
               "kind": "customers#customers",
               "etag": "\"hmpYzANfZzTNzlJGZ0MhW2o5M9g/7wYsqFUlssxhDsKu-s74w0Twaf0\""
              }
          });
        }, 1000);
       
    		return p.promise;
      }
      
      function getContracts(customerId){
          var p = $q.defer();
          $timeout(function() {
            p.resolve(generateContract(customerId));
          }, 1000);
          return p.promise;
      }

  }

  return customerEndpointService;
});
