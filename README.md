# cs-frontend-prototype

## Prerequisites

* Make sure you have npm installed

##Setting up

* Run `npm install` to install all dev tools
    - `bower install` and `grunt init` were already automated under this step

## Develop

* Run `grunt serve`

## Grunt tasks

* `grunt init` = blah
* `grunt compile-style` = blah
* `grunt serve` = blah
* `grunt test` = blah
* `grunt release` = blah
* `grunt build-pages` = blah